import React from 'react';

export default class AddTransaction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            transactionNumber: '',
            transactionType: '',
            amount: 0
        };
    }

    handleNumber = (el) => {
        this.setState({
            transactionNumber: el.target.value
        })
    }

    render() {
        return (
            <div>
                <input id="transaction-number" onChange={this.handleNumber} name="transaction-number" />
                <input id="transaction-type" onChange={this.handleType} name="transaction-type" />
                <input id="transaction-amount" onChange={this.handleAmount} name="transaction-amount" />
                <button value="add transaction" onClick={this.handleAdd}></button>

            </div>
        );
    }


    handleAdd = () => {
        let item = { ...this.state };
        this.props.itemAdded(item);
    }
}