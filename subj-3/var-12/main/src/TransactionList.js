import React from 'react';
import AddTransaction from './AddTransaction';
export default class BookList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
        this.addItem = (item) => {
            this.setState(prev => ({
                data: [...prev.data, item]
            }))
        }
    }

    render() {
        return (
            <div>
                <AddTransaction itemAdded={this.addItem} />
            </div>
        );
    }
}